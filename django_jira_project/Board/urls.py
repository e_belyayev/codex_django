from django.conf.urls import url
from Board import views as board_views

urlpatterns = [

    url(r'^project',
        board_views.create_project, 
        name='create_project'),
    url(r'^project/(?P<project_id>[0-9]+)/task',
        board_views.create_project, 
        name='create_task'),
    url(r'^project/(?P<project_id>[0-9]+)/(?P<task_id>[0-9]+)',
        board_views.create_project, 
        name='task_details'),
    url(r'^(?P<user_id>[0-9]+)/projects',
        board_views.create_project, 
        name='list_projects'),


]
